# Demo Shopfront Application

This project is a simple .net core application built to be hosted in aws lambda. It uses serverless (https://serverless.com ) framework to build and deploy.

This application doesn't use any persistence (such as RDS) on purpose. This can be added as additional adaptor if and when required.

## Debugging Locally using Visual Studio:

The project make use of three enviroment variables [API_ACESS_TOKEN_NAME, API_ACESS_TOKEN, REMOTE_API_URLBASE]. If these are not setup correctly, the application will not work.

In aws lambda environment, it is injected via Parameter store during deployment (see serverless.yml), however when debugging locally via Visual Studio, these can be set as Environment variables in ShopFront.WebApi.csproj properties -> Debug ->Environment variables.  


## Building, Packaging and Deploying to aws lambda:

Install serverless if not already installed.
```
    npm install -g serverless
```

Execute unit tests
```
    cd "ShopFront.Application.Tests"
    dotnet test
```

Deploy application
```
    ./build.cmd
    serverless deploy
```
Note: The last command above (`serverless deploy`) requires aws credentials to be setup. This project also contains `build.sh` which is intended to be used while deploying in Linux or MacOS machines, however it isn't tested. If it doesn't work, minor changes might be required.