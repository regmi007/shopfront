﻿using AutoMapper;
using ShopFront.Domain.Entities;
using ShopFront.ServiceContracts.Dtos;

namespace ShopFront.Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<ShopperHistory, ShopperHistoryDto>();
        }
    }
}
