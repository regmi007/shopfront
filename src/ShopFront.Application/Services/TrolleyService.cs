﻿using Newtonsoft.Json;
using ShopFront.ServiceContracts.Dtos;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ShopFront.Application.Services
{
    public class TrolleyService : ITrolleyService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public TrolleyService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        public async Task<decimal> CalculateTotal(TrolleyRequest request)
        {
            // Ideally in larger application I would rather encapsulate these magic strings rather than use them directly like this but 
            // for the scope of this exercise I used it this way just save some time and not over engineer.
            var httpClient = _httpClientFactory.CreateClient("Default"); 
            var httpContent = new StringContent(JsonConvert.SerializeObject(request), System.Text.Encoding.UTF8, "application/json-patch+json");

            var response = await httpClient.PostAsync(GetRequestUri("trolleyCalculator"), httpContent);
            var content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(content);
            }
            var total = JsonConvert.DeserializeObject<decimal>(content);
            return total;
        }

        private string GetRequestUri(string resource) => $"{resource}?token={Environment.GetEnvironmentVariable("API_ACESS_TOKEN")}";

    }
}

