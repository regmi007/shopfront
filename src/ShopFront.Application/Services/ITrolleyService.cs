﻿using ShopFront.ServiceContracts.Dtos;
using System;
using System.Threading.Tasks;

namespace ShopFront.Application.Services
{
    public interface ITrolleyService
    {
        Task<Decimal> CalculateTotal(TrolleyRequest request);
    }
}
