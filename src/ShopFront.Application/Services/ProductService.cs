﻿using AutoMapper;
using ShopFront.Domain.Queries;
using ShopFront.ServiceContracts.Dtos;
using ShopFront.ServiceContracts.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ShopFront.Application.ProductSorters;
using ShopFront.Domain.Entities;
using Autofac.Features.Indexed;

namespace ShopFront.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductQuery _productQuery;
        private readonly IMapper _mapper;
        private readonly IIndex<SortOptions, IProductSorter> _productSorter;

        public ProductService(
            IProductQuery productQuery,
            IMapper mapper,
            IIndex<SortOptions, IProductSorter> productSorter)
        {
            _productQuery = productQuery;
            _mapper = mapper;
            _productSorter = productSorter;
        }
        public async Task<IReadOnlyCollection<ProductDto>> GetProductsAsync(SortOptions sortOption)
        {
            var products = (await _productQuery.AllProductsAsync()).ToList();

            IComparer<Product> comparer = _productSorter[sortOption];
            products.Sort(comparer);

            return _mapper.Map<IReadOnlyCollection<ProductDto>>(products);
        }
    }
}
