﻿using ShopFront.ServiceContracts.Dtos;
using ShopFront.ServiceContracts.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopFront.Application.Services
{
    public interface IProductService
    {
        Task<IReadOnlyCollection<ProductDto>> GetProductsAsync(SortOptions sortOption);       
    }
}
