﻿using ShopFront.Domain.Entities;

namespace ShopFront.Application.ProductSorters
{
    public class HighToLowProductSorter : IProductSorter
    {
        public int Compare(Product x, Product y) => (y?.Price ?? 0).CompareTo(x?.Price ?? 0);
    }
}
