﻿using System;
using ShopFront.Domain.Entities;

namespace ShopFront.Application.ProductSorters
{
    public class AscendingByNameProductSorter : IProductSorter
    {
        public int Compare(Product x, Product y) => 
            String.Compare((x?.Name ?? string.Empty), y?.Name ?? string.Empty, StringComparison.InvariantCulture);
    }
}
