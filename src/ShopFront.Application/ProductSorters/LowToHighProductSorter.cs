﻿using ShopFront.Domain.Entities;

namespace ShopFront.Application.ProductSorters
{
    public class LowToHighProductSorter : IProductSorter
    {
        public int Compare(Product x, Product y) => (x?.Price ?? 0).CompareTo(y?.Price ?? 0);
    }
}
