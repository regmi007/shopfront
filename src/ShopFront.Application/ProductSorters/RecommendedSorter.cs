﻿using ShopFront.Domain.Entities;
using ShopFront.Domain.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopFront.Application.ProductSorters
{
    public class RecommendedSorter : IProductSorter
    {
        private static Dictionary<string, int> _productPopularity;
        private static readonly object Lock = new object();
        private readonly IProductQuery _productQuery;

        public RecommendedSorter(IProductQuery productQuery)
        {
            _productQuery = productQuery;
            // TODO: This not ideal way of doing it. 
            Task.Run(BuildProductPopularity).Wait();
        }

        private async Task BuildProductPopularity()
        {

            if (_productPopularity == null)
            {
                var history = await _productQuery.GetShopperHistoryAsync();
                lock(Lock)
                {
                    _productPopularity = history.SelectMany(x => x.Products)
                       .GroupBy(k => k.Name)
                       .Select(g => new {ProductName = g.Key, Popularity = g.Count() })
                       .ToDictionary(d => d.ProductName, v => v.Popularity);
                }               
            }
        }

        public int Compare(Product x, Product y)
        {
            _productPopularity.TryGetValue(x.Name, out var xPopularity);
            _productPopularity.TryGetValue(y.Name, out var yPopularity);

            return yPopularity.CompareTo(xPopularity);
        }
    
    }
}
