﻿using ShopFront.Domain.Entities;
using System.Collections.Generic;

namespace ShopFront.Application.ProductSorters
{
    public interface IProductSorter : IComparer<Product>
    {
    }
}
