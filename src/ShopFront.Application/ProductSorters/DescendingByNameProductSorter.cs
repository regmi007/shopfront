﻿using System;
using ShopFront.Domain.Entities;

namespace ShopFront.Application.ProductSorters
{
    public class DescendingByNameProductSorter : IProductSorter
    {
        public int Compare(Product x, Product y) => 
            String.Compare((y?.Name ?? string.Empty), x?.Name ?? string.Empty, StringComparison.InvariantCulture);
    }
}
