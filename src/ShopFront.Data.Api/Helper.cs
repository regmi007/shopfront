﻿using System.Net.Http;

namespace ShopFront.Data.Api
{
    public class Helper
    {
        public static void ThrowOnRemoteException(HttpResponseMessage response, string content)
        {
            if (!response.IsSuccessStatusCode)
            {
                throw new ApiException
                {
                    StatusCode = (int) response.StatusCode,
                    Content = content
                };
            }
        }
    }
}
