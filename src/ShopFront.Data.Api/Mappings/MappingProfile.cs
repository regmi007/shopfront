﻿using AutoMapper;
using ShopFront.Domain.Entities;
using ShopFront.ServiceContracts.Dtos;

namespace ShopFront.Data.Api.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ProductDto, Product>();
            CreateMap<ShopperHistoryDto, ShopperHistory>();
        }
    }
}
