﻿using System;

namespace ShopFront.Data.Api
{
    public class ApiException : Exception
    {
        public int StatusCode { get; set; }
        public string Content { get; set; }
    }
}
