﻿using AutoMapper;
using Newtonsoft.Json;
using ShopFront.Domain.Entities;
using ShopFront.Domain.Queries;
using ShopFront.ServiceContracts.Dtos;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ShopFront.Data.Api
{
    public class ProductQuery :IProductQuery
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IMapper _mapper;

        public ProductQuery(IMapper mapper,
            IHttpClientFactory httpClientFactory)
        {
            _mapper = mapper;
            _httpClientFactory = httpClientFactory;
        }
        private string GetRequestUri(string resource) => $"{resource}?token={Environment.GetEnvironmentVariable("API_ACESS_TOKEN")}";
        private async Task<T> GetResourceFromApiAsync<T>(string resource)
        {
            // Ideally in larger application I would rather encapsulate these magic strings rather than use them directly like this but 
            // for the scope of this exercise I used it this way just save some time and not over engineer.
            var response =  await _httpClientFactory.CreateClient("Default").GetAsync(GetRequestUri(resource));
            var content = await response.Content.ReadAsStringAsync();

            Helper.ThrowOnRemoteException(response, content);
            
            var products = JsonConvert.DeserializeObject<T>(content);
            return products;
        }

        public async Task<IEnumerable<Product>> AllProductsAsync()
        {
            var products = await GetResourceFromApiAsync<IEnumerable<ProductDto>>("products");
            return _mapper.Map<IEnumerable<Product>>(products);
        }

        public async Task<IEnumerable<ShopperHistory>> GetShopperHistoryAsync()
        {
            var shopperHistory = await GetResourceFromApiAsync<IEnumerable<ShopperHistoryDto>>("shopperHistory");
            return _mapper.Map<IEnumerable<ShopperHistory>>(shopperHistory);

        }        
    }
}
