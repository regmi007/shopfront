#!/bin/bash

#install zip on debian OS, since microsoft/dotnet container doesn't have zip by default
if [ -f /etc/debian_version ]
then
  apt -qq update
  apt -qq -y install zip
fi

dotnet restore
pushd ./shopFront.WebApi
dotnet lambda package --configuration release --framework netcoreapp2.1 --output-package ../bin/release/netcoreapp3.1/shopFront.zip
popd
