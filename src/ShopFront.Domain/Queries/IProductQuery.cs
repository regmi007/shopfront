﻿using ShopFront.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopFront.Domain.Queries
{
    public interface IProductQuery
    {
        Task<IEnumerable<Product>> AllProductsAsync();                
        Task<IEnumerable<ShopperHistory>> GetShopperHistoryAsync();                
    }
}
