﻿using System.Collections.Generic;

namespace ShopFront.Domain.Entities
{
    public class ShopperHistory
    {
        private ShopperHistory() { }

        public ShopperHistory(int customerId, IEnumerable<Product> product)
        {
            CustomerId = customerId;
            Products = product;
        }

        public int CustomerId { get; private set; }

        public IEnumerable<Product> Products { get; private set; }
    }
}
