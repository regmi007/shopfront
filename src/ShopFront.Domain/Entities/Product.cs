﻿namespace ShopFront.Domain.Entities
{
    public class Product
    {
        private Product() { }
        public Product(string name, decimal price, decimal quantity)
        {
            Name = name;
            Price = price;
            Quantity = quantity;
        }
        public string Name { get; private set; }

        public decimal Price { get; private set; }

        public decimal Quantity { get; private set; }

    }
}
