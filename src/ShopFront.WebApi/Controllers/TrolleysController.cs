﻿using Microsoft.AspNetCore.Mvc;
using ShopFront.Application.Services;
using ShopFront.ServiceContracts.Dtos;
using System.Threading.Tasks;

namespace ShopFront.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TrolleysController : Controller
    {
        private readonly ITrolleyService _trolleyService;

        public TrolleysController(ITrolleyService trolleyService)
        {
            _trolleyService = trolleyService;
        }

        [HttpPost]
        [Route("trolleyTotal")]
        public async Task<IActionResult> TrolleyTotal([FromBody] TrolleyRequest request)
        {
            var total = await _trolleyService.CalculateTotal(request);
            return Ok(total);
        }
    }
}