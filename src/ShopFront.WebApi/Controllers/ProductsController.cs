﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShopFront.Application.Services;
using ShopFront.ServiceContracts.Enums;

namespace ShopFront.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        [Route("sort")]
        public async Task<IActionResult> Get([FromQuery] string sortOption)
        {
            if(Enum.TryParse(sortOption, true, out SortOptions option))
            {
                var products = await _productService.GetProductsAsync(option);
                return products != null ? Ok(products) : (IActionResult) NotFound();
            }
            return UnprocessableEntity($"{sortOption} is not supported.");
        }
    }
}