﻿using System;
using Microsoft.AspNetCore.Mvc;
using ShopFront.ServiceContracts.Dtos;

namespace ShopFront.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        [HttpGet]
        [Route("user")]
        public IActionResult Get()
        {
            var token = Environment.GetEnvironmentVariable("API_ACESS_TOKEN");
            var name = Environment.GetEnvironmentVariable("API_ACESS_TOKEN_NAME");
            return Ok(new UserDto {Name =name, Token = token });
        }
    }
}