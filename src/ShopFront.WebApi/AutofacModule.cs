﻿using Autofac;
using ShopFront.Application.ProductSorters;
using ShopFront.Application.Services;
using ShopFront.Data.Api;
using ShopFront.Domain.Queries;
using ShopFront.ServiceContracts.Enums;

namespace ShopFront.WebApi
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<ProductQuery>().As<IProductQuery>();
            builder.RegisterType<ProductService>().As<IProductService>();
            builder.RegisterType<TrolleyService>().As<ITrolleyService>();

            RegisterProductSorters(builder);

        }

        private static void RegisterProductSorters(ContainerBuilder builder)
        {
            builder.RegisterType<RecommendedSorter>().Keyed<IProductSorter>(SortOptions.Recommended);
            builder.RegisterType<LowToHighProductSorter>().Keyed<IProductSorter>(SortOptions.Low);
            builder.RegisterType<HighToLowProductSorter>().Keyed<IProductSorter>(SortOptions.High);
            builder.RegisterType<AscendingByNameProductSorter>().Keyed<IProductSorter>(SortOptions.Ascending);
            builder.RegisterType<DescendingByNameProductSorter>().Keyed<IProductSorter>(SortOptions.Descending);
        }
    }
}
