﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Polly;
using System;
using ApplicationMappingProfile = ShopFront.Application.Mappings.MappingProfile;
using DataMappingProfile = ShopFront.Data.Api.Mappings.MappingProfile;

namespace ShopFront.WebApi
{
    public static class ConfigurationExtensions
    {
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ApplicationMappingProfile());
                mc.AddProfile(new DataMappingProfile());
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        public static void EnableCamelCaseJsonSerialization(this IServiceCollection services)
        {
            services
                .AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.PropertyNamingPolicy = null;
                    options.JsonSerializerOptions.DictionaryKeyPolicy = null;
               });
        }

        public static void AddAndConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ShopFront API", Version = "v1" });
            });
        }

        public static void AddAndConfigreHttpClientWithRetries(this IServiceCollection services, string profileName, string baseAddress)
        {
            services.AddHttpClient(profileName, client => {
                client.BaseAddress = new Uri(baseAddress);
            })
                .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
                {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(2)
                }));
        }
    }
}
