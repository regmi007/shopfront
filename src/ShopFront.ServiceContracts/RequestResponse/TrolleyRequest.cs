﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopFront.ServiceContracts.Dtos
{
    public class TrolleyRequest
    {
        public List<ProductDto> Products { get; set; }
        public List<SpecialsDto> Specials { get; set; }
        public List<QuantityDto> Quantities { get; set; }
    }
}
