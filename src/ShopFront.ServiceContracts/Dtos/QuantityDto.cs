﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopFront.ServiceContracts.Dtos
{
    public class QuantityDto
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
