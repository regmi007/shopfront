﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopFront.ServiceContracts.Dtos
{
    public class SpecialsDto
    {
        public List<QuantityDto> Quantities { get; set; }
        public decimal Total { get; set; }
    }
}
