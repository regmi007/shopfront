﻿namespace ShopFront.ServiceContracts.Dtos
{
    public class UserDto
    {
        public string Name { get; set; }
        public string Token { get; set; }
    }
}
