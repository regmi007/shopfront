﻿using System.Collections.Generic;

namespace ShopFront.ServiceContracts.Dtos
{
    public class ShopperHistoryDto
    {
        public int CustomerId { get; set; }

        public IEnumerable<ProductDto> Products { get; set; }
    }
}
