﻿namespace ShopFront.ServiceContracts.Enums
{
    public enum SortOptions
    {
        Low,
        High,
        Ascending,
        Descending,
        Recommended
    }
}
