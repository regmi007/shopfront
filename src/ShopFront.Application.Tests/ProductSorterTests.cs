using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using NUnit.Framework;
using ShopFront.Application.ProductSorters;
using ShopFront.Domain.Entities;
using ShopFront.Domain.Queries;

namespace ShopFront.Application.Tests
{
    [TestFixture]
    public class ProductSorterTests
    {
        private static readonly List<Product> Source = new List<Product> 
            {
                new Product("E", 6.0m, 1.0m),
                new Product("F", 5.0m, 1.0m),
                new Product("D", 4.0m, 1.0m),
                new Product("B", 3.0m, 1.0m),
                new Product("C", 2.0m, 1.0m),
                new Product("A", 1.0m, 1.0m)
            };

        private static IEnumerable<TestCaseData> _priceSorterTestCases =
            new List<TestCaseData> {
                new TestCaseData(new LowToHighProductSorter(), new[] { 1.0m, 2.0m, 3.0m, 4.0m, 5.0m, 6.0m }),
                new TestCaseData(new HighToLowProductSorter(), new[] { 6.0m, 5.0m, 4.0m, 3.0m, 2.0m, 1.0m })
            };

        [TestCaseSource(nameof(_priceSorterTestCases))]
        public void GivenUnSortedDataWhenSortByPriceItShouldSortCorrectly(IComparer<Product> comparer, decimal[] expectValue)
        {

            var testData = new List<Product>();
            testData.AddRange(Source);

            testData.Sort(comparer);
            Assert.IsTrue(Enumerable.SequenceEqual(expectValue, testData.Select(p => p.Price).ToArray()));
        }

        private static IEnumerable<TestCaseData> _nameSorterTestCases =
            new List<TestCaseData> {
                new TestCaseData(new AscendingByNameProductSorter(), new[] { "A", "B", "C", "D", "E", "F" }),
                new TestCaseData(new DescendingByNameProductSorter(), new[] {"F", "E", "D", "C", "B", "A" })
            };
        [TestCaseSource(nameof(_nameSorterTestCases))]
        public void GivenUnSortedDataWhenSortByNameItShouldSortCorrectly(IComparer<Product> comparer, string[] expectValue)
        {
            var testData = new List<Product>();
            testData.AddRange(Source);

            testData.Sort(comparer);
            Assert.IsTrue(Enumerable.SequenceEqual(expectValue, testData.Select(p => p.Name).ToArray()));
        }

        private static readonly List<ShopperHistory> ShopperHistorySource = new List<ShopperHistory>
        {
            new ShopperHistory(1, new List<Product> { new Product("C", 2.0m, 1), new Product("A", 1.0m, 1) }),
            new ShopperHistory(2, new List<Product> { new Product("C", 1.0m, 1), new Product("F", 5.0m, 1), new Product("A", 1.0m, 1) }),
            new ShopperHistory(3, new List<Product> { new Product("C", 1.0m, 1), new Product("F", 5.0m, 1), new Product("E", 5.0m, 1) }),
        };

        [Test]
        public void GivenUnSortedDataWhenSortedByRecommendedItShouldSortCorrectly()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var productQueryMock = fixture.Freeze<Mock<IProductQuery>>();
            productQueryMock.Setup(x => x.GetShopperHistoryAsync()).ReturnsAsync(ShopperHistorySource);

            var testData = new List<Product>();
            testData.AddRange(Source);

            var sorter = fixture.Create<RecommendedSorter>();
            testData.Sort(sorter);

            //Assert the query is called only once.
            productQueryMock.Verify(x => x.GetShopperHistoryAsync(), Times.Once);
            
            //Assert the product list is sorted as expected.
            //Array.Sort uses unstable sorting, thus excluding non value or same value items for assertion
            var expectValue = new[] { "C", "F", "A", "E" };
            Assert.IsTrue(Enumerable.SequenceEqual(expectValue, testData.Take(4).Select(p => p.Name).ToArray()));

        }
    }
}