﻿using NUnit.Framework;
using ShopFront.ServiceContracts.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace ShopFront.Application.Tests
{
    [TestFixture]
    public class TrolleyServiceTests
    {
    }

    public class TrolleyServiceTestData
    {
        public TrolleyRequest Request
        {
            get
            {
                return new TrolleyRequest
                {
                    Products = new List<ProductDto> {
                        new ProductDto {Name = "sand", Price = 2.0m},
                        new ProductDto {Name = "paper", Price = 3.0m},
                        new ProductDto {Name = "chocolate", Price = 4.0m}
                    },
                    Quantities = new List<QuantityDto>
                     {
                         new QuantityDto {Name = "paper", Quantity = 3 }
                     },
                    Specials = new List<SpecialsDto>
                     {
                        new SpecialsDto
                        {
                            Quantities = new List<QuantityDto>
                            {
                                new QuantityDto { Name = "paper", Quantity = 2 }
                            },
                            Total = 5.0m
                        }
                     }
                };
            }
        }
    }
}
